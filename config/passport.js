var LocalStrategy = require('passport-local').Strategy
var User = require('../models/user')



module.exports = function (passport) {
  // configure passport LocalStrategy used for user authentication
  passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
  },
  function (email, password, done) {
    User.findOne({ email: email }, function (err, user) {
      if (err) { return done(null, false, {user: null, validpass: user.validPassword(password), active: user.activated, found: false}) }
      if (user) {
        // found user check if password is valid
        if (!user.validPassword(password)) { return done(null, false, {user: null, validpass: user.validPassword(password), active: user.activated, found: true}) }
        // check if user has been verified
        if (!user.verified) { return done(null, false, {user: null, validpass: user.validPassword(password), active: user.activated, found: true}) }
      } else {
        // user not found
        return done(null, false, {user: null, validpass: false, active: false, found: false})
      }
      return done(null, user)
    })
  }
))


// return passport.authenticate
  return {
    // passport.authenticate for LocalStrategy wraped in a handler
    isAuthenticatedLocal: function (req, res) {
      // passport.authenticate with custom callback
      passport.authenticate('local', {session: false}, function (err, user, info) {
        if (err) { return res.json({ message: 'user authentication failed', data: err, success: false }) }
        // user not found
        if (!user) { return res.json({ message: 'user authentication failed', data: info, success: false }) }
        // authentication successful
        return res.json({message: 'user authentication successful', data: {user: user, validpass: true, active: true, found: true}, success: true})
      })(req, res)
    }
  }
}
