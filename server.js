var express = require('express')
var mongoose = require('mongoose')
var passport = require('passport')
var bodyParser = require('body-parser')
var app = express()

mongoose.connect(require('./config/database').url)

// create default api admin account if one is not found

app.use(express.static('public'))
app.use(bodyParser.raw({type: 'application/json'}))
app.use(bodyParser.json({extended: true})) // get JSON data
app.use(bodyParser.urlencoded({extended: true}))
app.use(passport.initialize())
app.use(app.router)
app.get('/', function (req, res) {
  res.sendfile('./public/index.html', {root: __dirname})
})

// load api routes
app.listen(process.env.OPENSHIFT_NODEJS_PORT || 80, process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0', function () {
  console.log('Server started')
})

